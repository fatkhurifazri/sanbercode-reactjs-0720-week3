import React from 'react';

class Name extends React.Component {
    render() {
        return <p>{this.props.nama}</p>
    }
}

class Price extends React.Component {
    render() {
        return <p>{this.props.harga}</p>
    }
}

class Weight extends React.Component {
    render() {
    return <p>{this.props.berat}</p>
    }
}

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
]

class Coba extends React.Component {
    render() {
        return (
            <>
                {dataHargaBuah.map((x) => {
                    return (
                        <tr>
                        <th><Name nama={x.nama}/> </th>
                        <th><Price harga={x.harga}/> </th>
                        <th><Weight berat={x.berat}/> </th>
                        </tr>
                    )
                })}
            </>
        )
    }
}

export default Coba


